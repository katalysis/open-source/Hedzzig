

import PackageDescription

let package = Package(
    name: "Hedzzig",
    dependencies: [
        .Package(url: "https://github.com/Zewo/TCP.git", majorVersion: 0, minor: 14),
        .Package(url: "https://github.com/krzyzanowskim/CryptoSwift.git", majorVersion: 0, minor: 8),
        .Package(url: "https://github.com/onevcat/AddressParser.git", majorVersion: 1),
        .Package(url: "https://github.com/onevcat/MimeType.git",  majorVersion: 1)
    ]
)
