//
//  SMTPSocket.swift
//  Hedwig
//
//  Created by Wei Wang on 2016/12/27.
//
//  Copyright (c) 2017 Wei Wang <onev@onevcat.com>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import Foundation
import Axis
import TCP
import COpenSSL
import CryptoSwift


struct SMTPSocket {
    
    
    public let connectionTimeout: Double
    public let requestTimeout: Double
    public let bufferSize: Int

    
    
    var stream: Axis.Stream
    
    init(stream: Axis.Stream, _ bufferSize: Int = 4096, _ connectionTimeout: Double = 60.seconds, _ requestTimeout: Double = 5.seconds) {
        self.stream = stream
        self.bufferSize = bufferSize
        self.connectionTimeout = connectionTimeout
        self.requestTimeout = requestTimeout
    }
    
    func connect() throws -> SMTPResponse {

        
        try stream.open(deadline: now() + connectionTimeout)
        
        let result = receive()
        
        guard result.count == 1 else {
            throw SMTP.SMTPError.badResponse
        }
        
    
        return try SMTPResponse(string: result[0])
    }
    func sendRaw(bytes: [UInt8], _ deadline: Double = 30.seconds) throws {
        logInTest("C: \(bytes)")
        
        try stream.write(bytes, deadline: deadline)
        try stream.flush(deadline: deadline)
    }
    
    
    func send(_ string: String, _ deadline: Double = 30.seconds) throws -> SMTPResponse {
        logInTest("C: \(string)")
        
        try stream.write((string + CRLF), deadline: deadline)
        try stream.flush(deadline: deadline)
        
        
        
        var parsed: SMTPResponse? = nil
        var response = receive()
        var multiple = false
        var result = [String]()
        
        // Workaround for different socket buffer implementation.
        // Some server prefer to buffer one line a time, while some 
        // other buffer all lines.
        if response.count == 1 {
            // One line case. Keep getting until can be parsed correctly.
            repeat {
                result.append(response[0])
                do {
                    parsed = try SMTPResponse(string: response[0])
                } catch {
                    multiple = true
                    response = receive()
                }
            } while parsed == nil
        } else {
            // Multiple lines. Try parse until get a correct line.
            multiple = true
            for res in response {
                result.append(res)
                if parsed == nil {
                    parsed = try? SMTPResponse(string: res)
                }
            } 
        }
        
        guard let parsedRes = parsed else {
            throw SMTP.SMTPError.badResponse
        }

        let r: SMTPResponse
        
        if multiple {
            r = SMTPResponse(code: parsedRes.code,
                             data: result.joined(separator: "\n"))
        } else {
            r = parsedRes
        }
        
        logInTest("S: \(r.data)")
        return r
    }
    
    func close() {
            stream.close()
    }
    
    func receive() -> [String] {
        let requestDeadline = now() + requestTimeout
        
        do {
            while !stream.closed {
                let chunk = try stream.read(upTo: bufferSize, deadline: requestDeadline)
    
                let text = try String(buffer: chunk)
                return text.seperated
            }
        } catch {
                
        }
        return []
    }

}


extension String {
    var seperated: [String] {
        return components(separatedBy: CRLF).filter { $0 != "" }
    }
}
