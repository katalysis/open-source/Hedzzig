import XCTest
@testable import HedzzigTests

XCTMain([
    testCase(HedzzigTests.allTests),
])
