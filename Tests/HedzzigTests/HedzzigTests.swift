import XCTest
import Venice
@testable import Hedzzig

class HedzzigTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        let smtpServer = "" 
        let smtpAcct = "smtp@katalysis.io"
        let smtpPwd = ""
        
        print("\(smtpServer):\(smtpAcct):\(smtpPwd)")
                
        let hedzzig = Hedzzig(hostName: smtpServer, user: smtpAcct, password: smtpPwd)
        
        let mail = Mail(
            text: "Test",
            from: "accounts@katalysis.io",
            to: "alex@katalysis.io",
            subject: "Link request to your Katalysis account"
        )
        
        
        
        hedzzig.send(mail) { error in
            if error != nil { print("Error: \(String(describing: error))") }
            else {
                print("completed")
            }
        }
        nap(for:5.seconds)

}


    static var allTests = [
        ("testExample", testExample),
    ]
}
